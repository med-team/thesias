<p align="center"><img src="https://raw.githubusercontent.com/daissi/thesias/master/misc/LogoThesias.png" alt="THESIAS"></p>

**THESIAS** (**Testing Haplotype EffectS In Association Studies**) is a popular software for carrying haplotype association analysis in unrelated individuals.
In addition to the command line interface, a graphical JAVA interface is now proposed allowing one to run THESIAS in a user-friendly manner.
Besides, new functionalities have been added to THESIAS including the possibility to analyze polychotomous phenotype and X-linked polymorphisms.

## Changes
- 2020/01/16: **3.1.1**
  - Clarify license with GPL-3+.
  - Update contact email address.
  - New makefile.

- 2007: **3.1**
  - [Download archive](https://github.com/daissi/thesias/releases/download/3.1/ThesiasPackage.zip)

## License
THESIAS is licensed under [GPL-3+](LICENSE).

## References
- D.A. Trégouët and V. Garelle, *Bioinformatics* (2007), [A new JAVA interface implementation of THESIAS: testing haplotype effects in association studies](https://dx.doi.org/10.1093/bioinformatics/btm058).
- D.A. Trégouët and L. Tiret, *European Journal of Human Genetics* (2004), [Cox proportional hazards survival regression in haplotype-based association analysis using the Stochastic-EM algorithm](https://dx.doi.org/10.1038/sj.ejhg.5201238).
- D.A. Trégouët et al., *Human Molecular Genetics* (2002), [Specific haplotypes of the P-selectin gene are associated with myocardial infarction](https://doi.org/10.1093/hmg/11.17.2015).
